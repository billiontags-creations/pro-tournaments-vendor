﻿// var domain = "http://192.168.1.11:8000/";
var domain = "http://api.citytournaments.in/";

//auth api
var register_api = domain + 'auth/register/';
var verifyotp_api = domain + 'auth/activate/account/';
var changephno_api = domain + 'auth/update/mobile-number/';
var login_api = domain + 'auth/login/';
var forgotpwd_api = domain + 'auth/forgot-password/';
var resetpassword_api = domain + 'auth/reset-password/';
var changepassword_api = domain + 'auth/change-password/';
var logout_api = domain + 'auth/logout/';

//add tournamnet apis 
var createtouranment_api = domain + 'tournament/create/';
var listtournaments_api = domain + 'tournament/manage/';
var retrievedetails_api = domain + 'tournament/retrieve/details/';

//edit page apis
var editphotos_api = domain + 'tournament/update/images/';
var edittournamnetdetails_api = domain + 'tournament/update/';
var edittournamnet_addr_api = domain + 'tournament/update/address/';
var edittournamnet_stadium_api = domain + 'tournament/update/stadium-details/';

var delrule_api = domain + "tournament/delete/category/";
var delorg_api = domain + "tournament/delete/organizer/";
var delprize_api = domain + "tournament/delete/prize/";

var editrules_api = domain + 'tournament/update/categories/';
var addrules_api = domain + 'tournament/create/categories/';

var editprice_api = domain + 'tournament/update/prizes/';
var addprice_api = domain + 'tournament/create/prizes/';

var editorg_api = domain + 'tournament/update/organizers/';
var addorg_api = domain + 'tournament/create/organizers/';

//bookings pag api
var listing_api = domain + 'booking/list/';

var deleteimg_api = domain + 'tournament/delete/images/';
