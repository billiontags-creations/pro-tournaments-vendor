$(function() {

    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });

    $(".addtrnmntldr").hide();

});

var addprod_images = [];

function addphotos(e) {
    if (window.File && window.FileList && window.FileReader) {
        var files = e.target.files;
        if (addprod_images.length + files.length < 4) {
            $('.close').click();
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                addprod_images.push(file);
                add_img_Files(file, i);
            }
            console.log(addprod_images);
        } else {
            $("#snackbarerror").text('Maximum of 3 photos are allowed');
            showerrtoast();
        }
    } else {
        alert("Your browser doesn't support to File API")
    }
}

function add_img_Files(f, i, type) {
    var reader = new FileReader();
    reader.onload = (function(file) {
        return function(e) {
            $(".allimages").append(`
            <div class="col-md-4 localimage">
                <img src="${e.target.result}" class="img-responsive h100" style="width:100%">
                <div class="play"> <a onclick="deletethisimage_local(this , '${file.name}' , ${file.lastModified})" class="bt_pointer"><img src="images/delete-button.svg" style="width: 28px;"></a> </div>
            </div>
        `);
        };
    })(f, i);
    reader.readAsDataURL(f);
}


//delete images locally fn starts here
function deletethisimage_local(el, fileName, lm) {
    addprod_images = addprod_images.filter(img => img.name != fileName && img.lastModified != lm);
    $(el).closest('.localimage').remove();
    console.log(addprod_images);
}
//add another price fn starts here
var anotherprize = 0;

function addanotherprize(number) {
    if ($('#mult_pricetag_' + number).val() == '') {
        $('#mult_pricetag_' + number).addClass('iserr');
        $("#snackbarerror").text("Price Tag is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    anotherprize++;

    $(".pricesaddrow").append(`<div class="form-group priceformgroup priceformgroup${anotherprize}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="col-md-12" for="mult_pricetag_${anotherprize}">Price Tag <span class="help"></span></label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="mult_pricetag_${anotherprize}" name="example-email" class="form-control ipadd price_name addprizeip" placeholder="Eg. 1st price"> </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-12">Price Amount</label>
                                                    <div class="col-md-12">
                                                        <input type="text" class="form-control ipadd price_amnt" id="mult_priceamnt_${anotherprize}" placeholder="Eg. Rs.5000"> </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="mt25">
                                                        <label class="col-md-12">Description</label>
                                                        <div class="col-md-12">
                                                            <textarea class="form-control resizenone ipadd price_desc" rows="2" id="mult_pricedesc_${anotherprize}"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mt25 priceaddicon${anotherprize}">
                                                        <a onclick="addanotherprize(${anotherprize})" class="plusicon cptr"><i class="ti-plus"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mt25 priceaddicon${anotherprize}">
                                                        <a onclick="removeprizerow(${anotherprize})" class="plusicon cptr"><i class="ti-trash"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`);

    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });
    $(".price_amnt").keypress(function(e) {
        if ($(this).val().length > 5 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}

function removeprizerow(rowno) {
    if($(".priceformgroup").length > 1){
        $(".priceformgroup" + rowno).remove();  
    }
}

//add organiser fn starts here
var anotherorganiser = 0;

function addorganiser(number) {
    if ($('#organiser_name_' + number).val() == '') {
        $('#organiser_name_' + number).addClass('iserr');
        $("#snackbarerror").text("Organiser Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#organiser_phone_' + number).val() == '') {
        $('#organiser_phone_' + number).addClass('iserr');
        $("#snackbarerror").text("Organiser Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    
    anotherorganiser++;

    $(".addorganiserrow").append(`<div class="form-group organiserfrmgrup organiserfrmgrup${anotherorganiser}">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label class="col-md-12" for="example-email">Name <span class="help"></span></label>
                                                        <div class="col-md-12">
                                                            <input type="email" id="organiser_name_${anotherorganiser}" name="example-email" class="form-control ipadd org_name addorgip" placeholder="Enter Name"> </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="col-md-12">Phone Number</label>
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control organiserphno ipadd org_phone addorgip" id="organiser_phone_${anotherorganiser}" placeholder="Enter Phone Number"> </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <label class="col-md-12" for="example-email">Email <span class="help"></span></label>
                                                        <div class="col-md-12">
                                                            <input type="email" id="organiser_email_${anotherorganiser}" name="example-email" class="form-control ipadd org_email" placeholder="Enter email"> </div>
                                                    </div>
                                                    <div class="col-md-2 organiseraddicon${anotherorganiser}">
                                                        <a onclick="addorganiser(${anotherorganiser})" class="plusicon cptr"><i class="ti-plus"></i></a>
                                                    </div>
                                                    <div class="col-md-2 organiseraddicon${anotherorganiser}">
                                                        <a onclick="removeorganiserrow(${anotherorganiser})" class="plusicon cptr"><i class="ti-trash"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                           `);
    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });
    $(".organiserphno").keypress(function(e) {
        if ($(this).val().length > 9 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}

function removeorganiserrow(rowno) {
    if($(".organiserfrmgrup").length > 1){
        $(".organiserfrmgrup" + rowno).remove();
    }
}

//addrules fn starts here
var anotherrules = 0;

function addrules(number) {
    if ($('#rules_category_' + number).val() == '') {
        $('#rules_category_' + number).addClass('iserr');
        $("#snackbarerror").text("Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#rules_price_' + number).val() == '') {
        $('#rules_price_' + number).addClass('iserr');
        $("#snackbarerror").text("Entry Fee is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    
    anotherrules++;
    $(".addrulesrow").append(` <div class="form-group rulesfrmgrup rulesfrmgrup${anotherrules}">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="mt25">
                                                        <label class="col-md-12">Category</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="rules_category_${anotherrules}" name="example-email" class="form-control rulescategory ipadd addrulesip" placeholder="Category">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="mt25">
                                                        <label class="col-md-12">Entry Fee</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="rules_price_${anotherrules}" name="example-email" class="form-control rulesprice ipadd addrulesip" placeholder="Entry Fee">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mt25 rulesaddicon${anotherrules}">
                                                        <a onclick="addrules(${anotherrules})" class="plusicon cptr"><i class="ti-plus"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mt25 rulesaddicon${anotherrules}">
                                                        <a onclick="removerulesrow(${anotherrules})" class="plusicon cptr"><i class="ti-trash"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`);
    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });
}

function removerulesrow(rowno) {
    if($(".rulesfrmgrup").length > 1){
        $(".rulesfrmgrup" + rowno).remove();
    }
}

var t_detailsid = ['tournament_name', 'tournament_sportsname', 'tournament_time',  'tournament_rules', 'tournament_stadname', 'tournament_street', 'tournament_area', 'tournament_city', 'tournament_state', 'tournament_country', 'tournament_zipcode'];

var t_detailserr = ['Tournament Name', 'Tournament Sports Name', 'Tournament Time', 'Rules', 'Stadium Name', 'Street', 'Area', 'City', 'State', 'Country', 'Zipcode'];


//add tournament services fn starts here
function addtournament_fn() {

    var postData = new FormData();

    if (addprod_images.length == 0) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#snackbarerror").text("Image is required");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    //tournament details validation , //address details validation

    for (var i = 0; i < t_detailsid.length; i++) {
        if ($('#' + t_detailsid[i] + '').val().trim() == '') {
            $("html, body").animate({ scrollTop: i < 4 ? 0 : 690 }, "slow");
            $('#' + t_detailsid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_detailserr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    //organiser details validation
    for(var i =0 ; i < $('.addorgip' ).length; i++) {
        if ($('.addorgip' ).eq(i).val() == '') {
            $("html, body").animate({ scrollTop:  $('.addorgip').eq(i).offset().top - 150 }, "slow");
            $('.addorgip').eq(i).addClass('iserr');
            $("#snackbarerror").text("Organiser detail is required");
            showerrtoast();
            event.preventDefault();
            return;
        }
    }
    

    for(var i = 0 ; i < $(".addrulesip").length; i++){
        if ($('.addrulesip').eq(i).val() == '') {
            $("html, body").animate({ scrollTop: $('.addrulesip').eq(i).offset().top -150 }, "slow");
            $('.addrulesip').eq(i).addClass('iserr');
            $("#snackbarerror").text("Category details is Required");
            showerrtoast();
            event.preventDefault();
            return;
        }    
    }


    //price tag validation
    for(var i = 0; i < $(".addprizeip").length; i++){
        if ($('.addprizeip').eq(i).val() == '') {
            $("html, body").animate({ scrollTop: $('.addprizeip').eq(i).offset().top - 150 }, "slow");
            $('.addprizeip').eq(i).addClass('iserr');
            $("#snackbarerror").text("Prize Tag is Required");
            showerrtoast();
            event.preventDefault();
            return;
        }
    }
    


    $(".addtrnmntldr").show();
    $(".addtrnmntBtn").attr("disabled", true);

    for (var i = 0; i < addprod_images.length; i++) {
        postData.append("images", addprod_images[i]);
    }

    postData.append("name", $('#tournament_name').val());
    var closingdate = $("#tournamentname_closing").val().split('/');
    var entry_closing_date = `${closingdate[2]}-${closingdate[0]}-${closingdate[1]}`;
    postData.append("entry_closing_date", entry_closing_date);
    var startdate = $("#tournamentname_date").val().split('-')[0];
    var enddate = $("#tournamentname_date").val().split('-')[1];
    startdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
    enddate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
    postData.append("start_date", startdate.replace(/ /g, ''));
    postData.append("end_date", enddate.replace(/ /g, ''));
    postData.append("time", $('#tournament_time').val());
    $('#tournament_maxentries').val() ? postData.append("max_entries", $('#tournament_maxentries').val()) : '';
    postData.append("sport", $('#tournament_sportsname').val());
    postData.append("rules", $('#tournament_rules').val());

    var address = {
        "street": $('#tournament_street').val(),
        "area": $('#tournament_area').val(),
        "city": $('#tournament_city').val(),
        "state": $('#tournament_state').val(),
        "country": $('#tournament_country').val(),
        "zipcode": $('#tournament_zipcode').val()
    };
    $('#tournament_landmark').val() ? postData.append("landmark", $('#tournament_landmark').val()) : '';
    postData.append("address", JSON.stringify(address));

    var stadium = { "name": $('#tournament_stadname').val() }
    postData.append("stadium", JSON.stringify(stadium));

    var prizes = [];
    $(".priceformgroup").each(function(){
        var o = { "tag": $(this).find(".price_name").val()};
        $(this).find(".price_amnt").val() ? o.amount = $(this).find(".price_amnt").val() : '';
        $(this).find(".price_desc").val() ? o.description = $(this).find(".price_desc").val() : '';
        prizes.push(o);
    });

    postData.append("prizes", JSON.stringify(prizes));

    var organizers = [];
    $(".organiserfrmgrup").each(function(){
        var ex = { "name": $(this).find(".org_name").val(), "contact_number": $(this).find(".org_phone").val() };
        $(this).find(".org_email").val() ? ex.email = $(this).find(".org_email").val() : '' ;
        organizers.push(ex);
    });
    postData.append("organizers", JSON.stringify(organizers));

    var rules = [];
    $(".rulesfrmgrup").each(function(){
        rules.push({ "category":  $(this).find$(".rulescategory").val(), "price":  $(this).find(".rulesprice").val() });
    })

    postData.append("categories", JSON.stringify(rules));

    $.ajax({
        url: createtouranment_api,
        type: 'post',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".addtrnmntldr").hide();
            $(".addtrnmntBtn").attr("disabled", false);
        },
        error: function(data) {
            $(".addtrnmntldr").hide();
            $(".addtrnmntBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {

        // $("#snackbarsuccs").text("Tournament Has Been Created! You will be contacted shortly");
        // showsuccesstoast();
        // setTimeout(function() { window.location.href = "http://protournaments-ang.billioncart.in/" }, 3000);
        swal("Tournament Has Been Created! You will be contacted shortly", {
          buttons: ["Close", "Go to Site"],
        });
        $(".swal-button.swal-button--confirm").css("background-color","#f0555e");
        $(".swal-button.swal-button--confirm").click(function() {
          window.location.href = "http://citytournaments.in";
        })

    }); //done fn ends here

}
