$(function() {

    getdetails();

    $(".picuploadLdr_1,.picuploadLdr_2,.t_detailssavldr,.t_details_addr_savldr,.t_details_stadium_savldr,.edit_rules_ldr,.add_rules_ldr,.edit_price_ldr,.add_price_ldr,.edit_org_ldr,.add_org_ldr").hide();

    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    });

});

var imageslength = 0;

function getdetails() {

    $.ajax({
        url: retrievedetails_api + sessionStorage.t_id + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            sessionStorage.t_details = JSON.stringify(data);
            $(".allimages").empty();
            imageslength = data.images.length;
            for (var i = 0; i < data.images.length; i++) {
                $(".allimages").append(`
                    <div class="col-md-4">
                        <div class="mediaimage">
                          <img src="${data.images[i].image}" class="img-responsive h100" style="width:100%">
                          <div class="overlay">
                            <div class="deletemark pointer" onclick='deleteimage(${data.images[i].id});'><img src="images/delete-button.svg" style="width:30px;height:30px"></div>
                          </div>
                        </div>
                    </div>
                `);
            }
            $("#tournament_name").val(data.name);
            $("#tournamentname_date").val("");
            $("#tournamentname_closing").val(data.entry_closing_date.split("-")[1]+"/"+data.entry_closing_date.split("-")[2]+"/"+data.entry_closing_date.split("-")[0]);
            $(".givendate").text(data.start_date + " To " + data.end_date);
            sessionStorage.startdate = data.start_date;
            sessionStorage.enddate = data.end_date;
            $("#tournament_time").val(data.time);
            $("#tournament_maxentries").val(data.max_entries ? data.max_entries: '');
            $("#tournament_sportsname").val(data.sport.name);
            // $("#tournament_rules").html(data.rules);
            $('.summernote').summernote('code', data.rules);
            $("#tournament_stadname").val(data.stadium.name);
            

            $("#tournament_street").val(data.address.street);
            sessionStorage.addressid = data.address.id;
            $("#tournament_area").val(data.address.area.name);
            $("#tournament_city").val(data.address.city);
            $("#tournament_state").val(data.address.state);
            $("#tournament_country").val(data.address.country);
            $("#tournament_zipcode").val(data.address.zipcode);
           
            data.address.landmark ? $("#tournament_landmark").val(data.address.landmark) : '';

            $(".dyn_rulesli").empty();
            for (var i = 0; i < data.categories.length; i++) {
                $(".dyn_rulesli").append(`<div class="col-md-12">
                                    <div class="contactaddress">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <p><b>Category :</b> <span>${data.categories[i].category}</span></p>
                                                <p><b>Price :</b> Rs <span>${data.categories[i].price}</span></p>
                                            </div>
                                            <div class="col-md-1"><a class="pointer addressanchor" data-toggle="modal" data-target="#editrulesmodal" onclick="getrulesdetails(${i} , ${data.categories[i].id})"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>
                                            <div class="col-md-1"><a class="pointer addressanchor" onclick="deleterulesdetails(${data.categories[i].id})"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>`);
            } //for loop ends here

            $(".dynpriceamountli").empty();
            for (var i = 0; i < data.prizes.length; i++) {
                $(".dynpriceamountli").append(`<li>
                                    <div class="bg-info"><a class="pointer" onclick="getpricedetails(${i} , ${data.prizes[i].id})" data-toggle="modal" data-target="#editpricingmodal"><i class="fa fa-pencil text-white"></i></a></div>
                                    <div class="bg-danger"><a class="pointer" onclick="deletepricedetails(${data.prizes[i].id})" ><i class="fa fa-trash text-white"></i></a></div>
                                   ${data.prizes[i].tag} </li>`);

            } //for loop ends here

            $(".dyn_organiserli").empty();
            for (var i = 0; i < data.organizers.length; i++) {
                $(".dyn_organiserli").append(`<div class="col-md-4 brline">
                                    <div class="contactaddress">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p><b>Name :</b> ${data.organizers[i].name}</p>
                                                <p><b>Email :</b> <span>${data.organizers[i].email ? data.organizers[i].email : 'Not Available' }</span></p>
                                                <p><b>Phone-no :</b> ${data.organizers[i].contact_number} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a data-toggle="modal" onclick="getorganiserdetails(${i} , ${data.organizers[i].id})" data-target="#editorganisermodal" class="editicon pointer"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="col-md-6">                             
                                            <a data-toggle="modal" onclick="deleteorganiserdetails(${data.organizers[i].id})"  class="plusicon pointer"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>`);
            }

        },
        error: function(data) {
            console.log('error occured in tournament');
        }
    });
}

function uploadphotos(type) {
    var postData = new FormData();
    if (addprod_images.length == 0) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#snackbarerror").text("Image is Required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        for (let i = 0; i < addprod_images.length; i++) {
            postData.append("images", addprod_images[i]);
        }
    }
    $(".picuploadLdr_" + type).show();
    $(".picuploadLdr_" + type).attr("disabled", true);

    $.ajax({
        url: editphotos_api + sessionStorage.t_id + '/',
        type: 'put',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".picuploadLdr_" + type).hide();
            $(".picuploadLdr_" + type).attr("disabled", false);
            getdetails();
        },
        error: function(data) {
            $(".picuploadLdr_" + type).hide();
            $(".picuploadLdr_" + type).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("Photos Updated Successfully!");
        showsuccesstoast();
        $(".photosmdlclse_" + type).click();
    }); //done fn ends here

}

var t_detailsid = ['tournament_name', 'tournament_time', 'tournament_sportsname', 'tournament_rules'];

var t_detailserr = ['Tournament Name', 'Tournament Time', 'Tournament Sports Name', 'Tournament Rules'];

// tournament details save fn starts here
function t_detailssave() {
    for (var i = 0; i < t_detailsid.length; i++) {
        if ($('#' + t_detailsid[i] + '').val() == '') {
            $('#' + t_detailsid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_detailserr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".t_detailssavldr").show();
    $(".t_detailssavBtn").attr("disabled", true);

    if ($("#tournamentname_date").val() != "") {
        var startdate = $("#tournamentname_date").val().split('-')[0];
        var enddate = $("#tournamentname_date").val().split('-')[1];
        sessionStorage.startdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
        sessionStorage.enddate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
    }

    if ($("#tournamentname_closing").val() != "") {
        var closingdate = $("#tournamentname_closing").val().split('/');
        var entry_closing_date = `${closingdate[2]}-${closingdate[0]}-${closingdate[1]}`;
    }

    var postData ={
        "name": $("#tournament_name").val(),
        "entry_closing_date": entry_closing_date,
        "start_date": sessionStorage.startdate.replace(/ /g, ''),
        "end_date": sessionStorage.enddate.replace(/ /g, ''),
        "time": $("#tournament_time").val(),
        "sport": $("#tournament_sportsname").val(),
        "rules": $("#tournament_rules").val()
    };
    $("#tournament_maxentries").val() ? postData.max_entries = $("#tournament_maxentries").val() : '';
    $.ajax({
        url: edittournamnetdetails_api + sessionStorage.t_id + '/',
        type: 'put',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".t_detailssavldr").hide();
            $(".t_detailssavBtn").attr("disabled", false);
            $(".givendate").text(sessionStorage.startdate.replace(/ /g, '') + " To " + sessionStorage.enddate.replace(/ /g, ''));
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".t_detailssavldr").hide();
            $(".t_detailssavBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });

} // tournament details save fn ends here

var t_addreid = ['tournament_street', 'tournament_area', 'tournament_city', 'tournament_state', 'tournament_country', 'tournament_zipcode'];
var t_addrerr = ['Street', 'Area', 'City', 'State', 'Country', 'Zipcode'];

var t_stadiumid = ['tournament_stadname'];
var t_stadiumerr = ['Stadium Name'];

function t_details_addr_save() {
    for (var i = 0; i < t_addreid.length; i++) {
        if ($('#' + t_addreid[i] + '').val() == '') {
            $('#' + t_addreid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_addrerr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".t_details_addr_savldr").show();
    $(".t_details_addr_savBtn").attr("disabled", true);

    var address = {
        "street": $('#tournament_street').val(),
        "area": $('#tournament_area').val(),
        "city": $('#tournament_city').val(),
        "state": $('#tournament_state').val(),
        "country": $('#tournament_country').val(),
        "zipcode": $('#tournament_zipcode').val()
    };
    
    $('#tournament_landmark').val() ? postData.append("landmark", $('#tournament_landmark').val()) : '';
    $.ajax({
        url: edittournamnet_addr_api + sessionStorage.addressid + '/',
        type: 'put',
        data: JSON.stringify(address),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".t_details_addr_savldr").hide();
            $(".t_details_addr_savBtn").attr("disabled", false);
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".t_details_addr_savldr").hide();
            $(".t_details_addr_savBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function t_details_stadium_save() {
    for (var i = 0; i < t_stadiumid.length; i++) {
        if ($('#' + t_stadiumid[i] + '').val() == '') {
            $('#' + t_stadiumid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_stadiumerr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".t_details_stadium_savldr").show();
    $(".t_details_stadium_savBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "name": $('#tournament_stadname').val()
    });

    $.ajax({
        url: edittournamnet_stadium_api + sessionStorage.t_id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".t_details_stadium_savldr").hide();
            $(".t_details_stadium_savBtn").attr("disabled", false);
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".t_details_stadium_savldr").hide();
            $(".t_details_stadium_savBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

// get rules details fn
function getrulesdetails(rowno, id) {
    sessionStorage.ruleid = id;
    var data = JSON.parse(sessionStorage.t_details);
    $("#edit_rulecategory").val(data.categories[rowno].category);
    $("#edit_ruleprice").val(data.categories[rowno].price);
}


function editrules_final() {

    if ($('#edit_rulecategory').val().trim() == '') {
        $('#edit_rulecategory').addClass("iserr");
        $("#snackbarerror").text("Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#edit_ruleprice').val().trim() == '') {
        $('#edit_ruleprice').addClass("iserr");
        $("#snackbarerror").text("Price is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".edit_rules_ldr").show();
    $(".edit_rules_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "category": $('#edit_rulecategory').val(),
        "price": $('#edit_ruleprice').val()
    });

    $.ajax({
        url: editrules_api + sessionStorage.ruleid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_rules_ldr").hide();
            $(".edit_rules_Btn").attr("disabled", false);
            $("#editrulesmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".edit_rules_ldr").hide();
            $(".edit_rules_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function addrules_final() {


    if ($('#add_rulecategory').val().trim() == '') {
        $('#add_rulecategory').addClass("iserr");
        $("#snackbarerror").text("Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#add_ruleprice').val().trim() == '') {
        $('#add_ruleprice').addClass("iserr");
        $("#snackbarerror").text("Price is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".add_rules_ldr").show();
    $(".add_rules_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "category": $('#add_rulecategory').val(),
        "price": $('#add_ruleprice').val()
    });

    $.ajax({
        url: addrules_api + sessionStorage.t_id + '/',
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_rules_ldr").hide();
            $(".add_rules_Btn").attr("disabled", false);
            $("#addrulesmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".add_rules_ldr").hide();
            $(".add_rules_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

// get price details fn
function getpricedetails(rowno, id) {
    sessionStorage.priceid = id;
    var data = JSON.parse(sessionStorage.t_details);
    $("#edit_pricetag").val(data.prizes[rowno].tag);
    $("#edit_priceamnt").val(data.prizes[rowno].amount);
    $("#edit_priceresult").val(data.prizes[rowno].winner_name);
    $("#edit_pricedesc").val(data.prizes[rowno].description);
}

function editprise_final() {

    if ($('#edit_pricetag').val().trim() == '') {
        $('#edit_pricetag').addClass("iserr");
        $("#snackbarerror").text("Price Tag is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    

    $(".edit_price_ldr").show();
    $(".edit_price_Btn").attr("disabled", true);

    var postData = {
        "tag": $('#edit_pricetag').val()
    };

    $('#edit_priceamnt').val() ? postData.amount = $('#edit_priceamnt').val() : '';
    $('#edit_priceresult').val() ? postData.winner_name = $('#edit_priceresult').val() : '';
    $('#edit_pricedesc').val() ? postData.description = $('#edit_pricedesc').val() : '';
    $.ajax({
        url: editprice_api + sessionStorage.priceid + '/',
        type: 'put',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_price_ldr").hide();
            $(".edit_price_Btn").attr("disabled", false);
            $("#editpricingmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".edit_price_ldr").hide();
            $(".edit_price_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function addprise_final() {

    if ($('#add_pricetag').val().trim() == '') {
        $('#add_pricetag').addClass("iserr");
        $("#snackbarerror").text("Price Tag is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    

    $(".add_price_ldr").show();
    $(".add_price_Btn").attr("disabled", true);

    var postData = {
        "tag": $('#add_pricetag').val()
    };

    $('#add_priceamnt').val() ? postData.amount =  $('#add_priceamnt').val() : '';
    $('#add_priceresult').val() ? postData.winner_name =  $('#add_priceresult').val() : '';
    $('#add_pricedesc').val() ? postData.description = $('#add_pricedesc').val() : '';
    $.ajax({
        url: addprice_api + sessionStorage.t_id + '/',
        type: 'post',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_price_ldr").hide();
            $(".add_price_Btn").attr("disabled", false);
            $("#addpricingmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".add_price_ldr").hide();
            $(".add_price_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function getorganiserdetails(rowno, id) {
    sessionStorage.organniseid = id;
    var data = JSON.parse(sessionStorage.t_details);
    $("#edit_orgname").val(data.organizers[rowno].name);
    $("#edit_orgphone").val(data.organizers[rowno].contact_number);
    $("#edit_orgemail").val(data.organizers[rowno].email);
}

function editorg_final() {

    if ($('#edit_orgname').val().trim() == '') {
        $('#edit_orgname').addClass("iserr");
        $("#snackbarerror").text("Organiser Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_orgphone').val().trim() == '') {
        $('#edit_orgphone').addClass("iserr");
        $("#snackbarerror").text("Organiser Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    // if ($('#edit_orgemail').val().trim() == '') {
    //     $('#edit_orgemail').addClass("iserr");
    //     $("#snackbarerror").text("Organiser Email ID is Required");
    //     showerrtoast();
    //     event.preventDefault();
    //     return;
    // }
    // var x = $('#edit_orgemail').val();
    // var atpos = x.indexOf("@");
    // var dotpos = x.lastIndexOf(".");
    // if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
    //     $('#edit_orgemail').addClass("iserr");
    //     $("#snackbarerror").text("Valid E-Mail Address is required");
    //     showiperrtoast();
    //     event.stopPropagation();
    //     return;
    // }

    $(".edit_org_ldr").show();
    $(".edit_org_Btn").attr("disabled", true);

    var postData = {
        "name": $('#edit_orgname').val(),
        "contact_number": $('#edit_orgphone').val()
    };
     $('#edit_orgemail').val() ? postData.email = $('#edit_orgemail').val() : '';
    $.ajax({
        url: editorg_api + sessionStorage.organniseid + '/',
        type: 'put',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_org_ldr").hide();
            $(".edit_org_Btn").attr("disabled", false);
            $("#editorganisermodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".edit_org_ldr").hide();
            $(".edit_org_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function addorg_final() {

    if ($('#add_orgname').val().trim() == '') {
        $('#add_orgname').addClass("iserr");
        $("#snackbarerror").text("Organiser Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_orgphone').val().trim() == '') {
        $('#add_orgphone').addClass("iserr");
        $("#snackbarerror").text("Organiser Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    // if ($('#add_orgemail').val().trim() == '') {
    //     $('#add_orgemail').addClass("iserr");
    //     $("#snackbarerror").text("Organiser Email ID is Required");
    //     showerrtoast();
    //     event.preventDefault();
    //     return;
    // }
    // var x = $('#add_orgemail').val();
    // var atpos = x.indexOf("@");
    // var dotpos = x.lastIndexOf(".");
    // if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
    //     $('#add_orgemail').addClass("iserr");
    //     $("#snackbarerror").text("Valid E-Mail Address is required");
    //     showiperrtoast();
    //     event.stopPropagation();
    //     return;
    // }

    $(".add_org_ldr").show();
    $(".add_org_Btn").attr("disabled", true);

    var postData = {
        "name": $('#add_orgname').val(),
        "contact_number": $('#add_orgphone').val()
    };
    $('#add_orgemail').val() ? postData.email = $('#add_orgemail').val() : '';
    $.ajax({
        url: addorg_api + sessionStorage.t_id + '/',
        type: 'POST',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_org_ldr").hide();
            $(".add_org_Btn").attr("disabled", false);
            $("#addorganisermodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".add_org_ldr").hide();
            $(".add_org_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

const deleteimage = id => {
    $.ajax({
        url: deleteimg_api,
        type: "delete",
        data: JSON.stringify({
            pk: [id]
        }),
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: data => {
            getdetails();
            $("#snackbarsuccs").text("Image Deleted Successfully");
            showsuccesstoast();
        },
        error: data => {
            for (var key in JSON.parse(data.responseText)) {
                jQuery("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

var addprod_images = [];

function addphotos(e) {
    if (window.File && window.FileList && window.FileReader) {
        var files = e.target.files;
        if (addprod_images + files.length < 4 - imageslength) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                addprod_images.push(file);
                add_img_Files(file, i);
            }
            console.log(addprod_images);
        } else {
            $("#snackbarerror").text('Maximum of 3 photos are allowed');
            showerrtoast();
        }
    }
}


function add_img_Files(f, i, type) {
    var reader = new FileReader();
    reader.onload = (function(file) {
        return function(e) {
            $(".modalimages").append(`
            <div class="col-md-4 localimage">
                <img src="${e.target.result}" class="img-responsive h100" style="width:100%">
                <div class="play"> <a onclick="deletethisimage_local(this , '${file.name}' , ${file.lastModified})" class="bt_pointer"><img src="images/delete-button.svg" style="width: 28px;"></a> </div>
            </div>
        `);
        };
    })(f, i);
    reader.readAsDataURL(f);
}

//delete images locally fn starts here
function deletethisimage_local(el, fileName, lm) {
    addprod_images = addprod_images.filter(img => img.name != fileName && img.lastModified != lm);
    $(el).closest('.localimage').remove();
    console.log(addprod_images);
}


function deleterulesdetails(id){
    $("#deleterulesmodal").modal("show");
    $(".delete_rules_Btn").attr("onclick", `deleterules_final(${id})`);
}

function deletepricedetails(id){
    $("#deleteprizemodal").modal("show");
    $(".delete_prize_Btn").attr("onclick", `deleteprize_final(${id})`);
}

function deleteorganiserdetails(id){
    $("#deleteorgmodal").modal("show");
    $(".delete_org_Btn").attr("onclick", `deleteorg_final(${id})`);
}

function deleterules_final(id){
    let ajaxsetting = {
        url : delrule_api + id + "/",
        type: "delete",
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        }
    };
    $(".delete_rules_Btn").attr("disabled", true);
    $(".delete_rules_ldr").show();
    $.ajax(ajaxsetting)
        .done(function(data){
            getdetails();
            $(".delete_rules_Btn").attr("disabled", false);
            $(".delete_rules_ldr").hide();
            $(".close").click();
            $("#snackbarsuccs").text("Category Deleted Successfully");
            showsuccesstoast();
        })
        .fail(function(data){
            $(".delete_rules_Btn").attr("disabled", false);
            $(".delete_rules_ldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                jQuery("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        })
}

function deleteprize_final(id){
    let ajaxsetting = {
        url : delprize_api + id + "/",
        type: "delete",
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        }
    };
    $(".delete_prize_Btn").attr("disabled", true);
    $(".delete_prize_ldr").show();
    $.ajax(ajaxsetting)
        .done(function(data){
            getdetails();
            $(".delete_prize_Btn").attr("disabled", false);
            $(".delete_prize_ldr").hide();
            $(".close").click();
            $("#snackbarsuccs").text("Prize detail Deleted Successfully");
            showsuccesstoast();
        })
        .fail(function(data){
            $(".delete_prize_Btn").attr("disabled", false);
            $(".delete_prize_ldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                jQuery("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        })
}

function deleteorg_final(id){
    let ajaxsetting = {
        url : delorg_api + id + "/",
        type: "delete",
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        }
    };
    $(".delete_org_Btn").attr("disabled", true);
    $(".delete_org_ldr").show();
    $.ajax(ajaxsetting)
        .done(function(data){
            getdetails();
            $(".delete_org_Btn").attr("disabled", false);
            $(".delete_org_ldr").hide();
            $(".close").click();
            $("#snackbarsuccs").text("Organiser detail Deleted Successfully");
            showsuccesstoast();
        })
        .fail(function(data){
            $(".delete_org_Btn").attr("disabled", false);
            $(".delete_org_ldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                jQuery("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        })
}